package com.michalrys.dedicatedpageclawlers.extractor;

public interface DataExtractor {
    void readWebPage(String urlAddress);

    StringBuilder extractData();
}
