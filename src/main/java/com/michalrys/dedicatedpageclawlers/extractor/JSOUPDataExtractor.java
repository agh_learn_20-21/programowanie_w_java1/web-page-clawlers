package com.michalrys.dedicatedpageclawlers.extractor;

import com.michalrys.dedicatedpageclawlers.extractor.algorithms.JSOUPPageExtractionAlgorithm;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.IOException;

public class JSOUPDataExtractor implements DataExtractor {
    private final JSOUPPageExtractionAlgorithm algorithm;
    private String urlAddress;
    private Document document;

    public JSOUPDataExtractor(JSOUPPageExtractionAlgorithm fourteenLOPageExtractionAlgorithm) {
        this.algorithm = fourteenLOPageExtractionAlgorithm;
    }

    @Override
    public void readWebPage(String urlAddress) {
        setUrlAddress(urlAddress);
        setDocumentFromByReadingWebPage();
    }

    private void setUrlAddress(String urlAddress) {
        this.urlAddress = urlAddress;
    }

    private void setDocumentFromByReadingWebPage() {
        Connection connect = Jsoup.connect(urlAddress);
        try {
            document = connect.get();
        } catch (IOException e) {
            document = new Document(urlAddress);
            document.text("Web page was not loaded.");
        }
    }

    @Override
    public StringBuilder extractData() {
        return algorithm.extract(urlAddress, document);
    }
}