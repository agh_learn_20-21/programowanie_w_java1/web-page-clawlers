package com.michalrys.dedicatedpageclawlers.extractor.algorithms;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class FourteenLOJSOUPPageSimplestArticlesExtractionAlgorithm implements JSOUPPageExtractionAlgorithm {
    @Override
    public StringBuilder extract(String urlAddress, Document doc) {
        // web page: http://www.xiv-lo.krakow.pl
        // articles have html tag: <article ...>
        // post title, date, author and content have specific class name

        StringBuilder parsedData = new StringBuilder();
        parsedData.append(urlAddress);
        parsedData.append("\n--------------------------------------------------------------------------------------\n");

        Elements posts = doc.getElementsByClass("dd-post");
        for (Element post : posts) {
            String postNameCheck = post.className();
            if (postNameCheck.equals("dd-post dd-messages")) {
                continue;
            }

            Elements headerPart = post.getElementsByClass("dd-postheader");
            for (Element header : headerPart) {
                if (header.text() != null) {
                    parsedData.append(header.text() + " : ");
                }
            }

            Elements postDatePart = post.getElementsByClass("dd-postdateicon");
            for (Element postDate : postDatePart) {
                if (postDate.text() != null) {
                    parsedData.append(postDate.text() + " : ");
                }
            }
            Elements authorPart = post.getElementsByClass("dd-postauthoricon");
            for (Element author : authorPart) {
                if (author.text() != null) {
                    parsedData.append(author.text() + "\n");
                }
            }
            Elements articlePart = post.getElementsByClass("dd-article");
            for (Element article : articlePart) {
                if (article.text() != null) {
                    parsedData.append(article.text() + "\n\n");
                }
            }
        }
        return parsedData;
    }
}