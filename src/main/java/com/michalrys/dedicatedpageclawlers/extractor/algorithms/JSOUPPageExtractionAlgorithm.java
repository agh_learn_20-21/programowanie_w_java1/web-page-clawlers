package com.michalrys.dedicatedpageclawlers.extractor.algorithms;

import org.jsoup.nodes.Document;

public interface JSOUPPageExtractionAlgorithm {
    StringBuilder extract(String urlAddress, Document doc);
}
