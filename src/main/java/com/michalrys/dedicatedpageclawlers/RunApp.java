package com.michalrys.dedicatedpageclawlers;

import com.michalrys.dedicatedpageclawlers.clawler.WebCrawlerExtractingArticlesFromFourteenLO;
import com.michalrys.dedicatedpageclawlers.clawler.WebPageCrawler;
import com.michalrys.dedicatedpageclawlers.extractor.algorithms.FourteenLOJSOUPPageSimplestArticlesExtractionAlgorithm;
import com.michalrys.dedicatedpageclawlers.printer.ResultsPrinterToFileWithUTF8;

import java.io.File;
import java.io.IOException;

public class RunApp {
    public static void main(String[] args) throws IOException {
        File outputFile = new File("./src/main/resources/content.txt");

        String pageForExtraction = "http://www.xiv-lo.krakow.pl/index.php?start=5";
//        String pageForExtraction = "http://xiv-lo.krakow.pl/index.php/rodo";
//        String pageForExtraction = "http://www.xiv-lo.krakow.pl/index.php";

        WebPageCrawler webCrawlerExtractingArticlesFromFourteenLO = new WebCrawlerExtractingArticlesFromFourteenLO(
                pageForExtraction,
                new FourteenLOJSOUPPageSimplestArticlesExtractionAlgorithm(),
                new ResultsPrinterToFileWithUTF8(outputFile.getAbsolutePath()));

        webCrawlerExtractingArticlesFromFourteenLO.run(outputFile.getAbsolutePath());
    }
}