package com.michalrys.dedicatedpageclawlers.clawler;

import java.io.IOException;

public interface WebPageCrawler {

    String getUrlAddress();

    void setUrlAddress(String urlAddress);

    void run(String filePath) throws IOException;
}
