package com.michalrys.dedicatedpageclawlers.clawler;

import com.michalrys.dedicatedpageclawlers.extractor.DataExtractor;
import com.michalrys.dedicatedpageclawlers.extractor.JSOUPDataExtractor;
import com.michalrys.dedicatedpageclawlers.extractor.algorithms.JSOUPPageExtractionAlgorithm;
import com.michalrys.dedicatedpageclawlers.printer.ResultsPrinter;

public class WebCrawlerExtractingArticlesFromFourteenLO implements WebPageCrawler {
    private String urlAddress;
    private final ResultsPrinter resultsPrinter;
    private final DataExtractor dataExtractor;

    public WebCrawlerExtractingArticlesFromFourteenLO(String urlAddress,
                                                      JSOUPPageExtractionAlgorithm algorithm,
                                                      ResultsPrinter resultsPrinter) {
        this.urlAddress = urlAddress;
        this.dataExtractor = new JSOUPDataExtractor(algorithm);
        this.resultsPrinter = resultsPrinter;
    }

    @Override
    public String getUrlAddress() {
        return urlAddress;
    }

    @Override
    public void setUrlAddress(String urlAddress) {
        this.urlAddress = urlAddress;
    }

    @Override
    public void run(String filePath) {
        dataExtractor.readWebPage(urlAddress);
        StringBuilder parsedData = dataExtractor.extractData();
        resultsPrinter.print(parsedData.toString());
    }
}