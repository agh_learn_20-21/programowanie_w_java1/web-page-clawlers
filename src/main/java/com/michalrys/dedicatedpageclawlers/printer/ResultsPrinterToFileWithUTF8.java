package com.michalrys.dedicatedpageclawlers.printer;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.nio.charset.StandardCharsets;

public class ResultsPrinterToFileWithUTF8 implements ResultsPrinter {
    private final String filePath;

    public ResultsPrinterToFileWithUTF8(String filePath) {
        this.filePath = filePath;
    }

    @Override
    public void print(String results) {
        try (BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(
                new FileOutputStream(filePath), StandardCharsets.UTF_8))) {
            writer.write(results);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}